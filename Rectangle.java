class Shape
{
   int width;
   int height;
   int length;
   void setWidth(int w)
   {
       width=w;
   }
   void setHeight(int h)
   {
       height=h;
   }
   void setLength(int l)
   {
       length=l;
   }
}
class Rectangle extends Shape
{
   int display()
   {
       return(width*height*length);
   }
   public static void main(String[]args)
   {
       Rectangle r=new Rectangle();
       r.setWidth(5);
       r.setHeight(7);
       r.setLength(2);
       System.out.println("Volume:"+r.display());
   }
}
