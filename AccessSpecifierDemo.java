class AccessSpecifierDemo{
    private
        int priVar;
    protected
        int proVar;
    public
        int pubVar;
    public
         void setVar(int priValue,int proValue, int pubValue){
            priVar = priValue;
            proVar = proValue;
            pubVar = pubValue;
         }




    public
        void getVar(){
            System.out.println("private variable is : "+priVar);
            System.out.println("protected variable is : "+proVar);
            System.out.println("public variable is : "+pubVar);  
        }
       
public static void main(String[]args){
    AccessSpecifierDemo obj=new AccessSpecifierDemo();
    obj.setVar(30,40,55);
    obj.getVar();




}
}
