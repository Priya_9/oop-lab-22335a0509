#include <iostream>
using namespace std;
class Rectangle {
private:
    int width;
    int height;
public:
    Rectangle(int w, int h) {
        width = w;
        height = h;
    }
    int getArea() {
        return width * height;
    }
};


int main() {
    Rectangle r(5, 10);
    int area = r.getArea();
    cout << "Area: " << area << endl;
    return 0;
}
