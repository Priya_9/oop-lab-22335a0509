class Child1
{
   int a=10;
   void display1()
   {
       System.out.println("Content in child1 is:"+a);
   }
}
//Simple Inheritance
class Child2 extends Child1
{
   int b=20;
   int c=a+b;
   void display2()
   {
       System.out.println("Content in child2 is:"+c);
   }
}
//Multilevel Inheritance
class Child3 extends Child2
{
   int d=30;
   int e=c+d;
   void display3()
   {
       System.out.println("Content in child3 is:"+e);
   }
}
//Hierarchical Inheritance
class Child4 extends Child1
{
   int f=40;
   void display4()
   {
       System.out.println("content in child4 is:"+(a+f));
   }
}
//Hybrid Inheritance
class Child5 extends Child2{
   int g=50;
   void display5()
   {
       System.out.println("Content in child5 is:"+(b+g));
   }
}
class Inherit{
   public static void main(String[]args)
{
   Child2 obj1=new Child2();
   obj1.display2();
   Child3 obj2=new Child3();
   obj2.display3();
   Child4 obj3=new Child4();
   obj3.display4();
   Child5 obj4=new Child5();
   obj4.display5();
}
}
