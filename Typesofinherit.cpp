 #include <iostream>
using namespace std;
class Child1
{
   public:
   int a=10;
   void display1()
   {
       cout<<"Content in child1 is"<<a<<endl;
   }
};
//Simple Inheritance
class Child2:public Child1
{
   public:
   int b=20;
   int c=a+b;
   void display2()
   {
       cout<<"Content in child2 is"<<c<<endl;
   }
};
//Multilevel Inheritance
class Child3:public Child2
{
   public:
   int d=30;
   int e=c+d;
   void display3()
   {
       cout<<"Content in child3 is"<<e<<endl;
   }
};
//Hierarchical Inheritance
class Child4:public Child1
{
   public:
   int f=40;
   void display4()
   {
       cout<<"content in child4 is"<<a+f<<endl;
   }
};
//Hybrid Inheritance
class Child5:public Child2{
   public:
   int g=50;
   void display5()
   {
       cout<<"Content in child5 is"<<b+g<<endl;
   }
};
int main()
{
   Child2 obj1;
   obj1.display2();
   Child3 obj2;
   obj2.display3();
   Child4 obj3;
   obj3.display4();
   Child5 obj4;
   obj4.display5();
}


