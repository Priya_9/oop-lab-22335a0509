class Parent{
    String fullname="Damini Priya";
    String surname="Karri";
}


class Child1 extends Parent{
    void display1(){
        System.out.println("My fullname is: " + fullname);
   
        System.out.println("My surname is: " + surname);
    }
}


public class Inheritance{
    public static void main(String[] args){
        Child1 obj1 = new Child1();
       
        obj1.display1();
   
       
    }
}
